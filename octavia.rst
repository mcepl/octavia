#######
Octavia
#######

:Author: Lucius Annaeus Seneca
:Translated: Watson Bradshaw
:Publisher: London: Swan Sonnenschein & Co., Paternoster Square
:Date: 1902

DRAMATIS PERSONAE
-----------------

 * OCTAVIA
 * POPPAEA
 * OCTAVIA’S NURSE
 * AGRIPPINA
 * CHORUS OF ROMANS
 * NERO
 * SENECA
 * MESSENGER
 * PREFECT

ARGUMENT
--------

CLAUDIUS DRUSUS CAESAR (Messalina, because she had married Silius, being
condemned to die—she had borne him (Caesar) Britannicus and Octavia)
took to himself, for a fourth wife (he had divorced Urgulanilla and
Aelia Paetina before he married Messalina). Agrippina, the daughter of
his brother Germanicus, and the widow of Cn. Domitius Aenobarbus Nero,
to whose son, he gave his daughter Octavia in marriage. Claudius and
Britannicus being poisoned, Nero, then Emperor, divorces Octavia, whom
he had always hated, and marries Poppaea Sabina; in consequence of which
divorce he had to put down the riots amongst the populace amidst great
slaughter, and he orders Octavia to be transported to Pandataria, and
there to be slain.

ACT I.
------

:OCTAVIA: (Octavia, weary of her existence, bewails her misery.) Aurora,
          that was shining brilliantly in the heavens, is now forsaking
          the wandering starry group, and Titan is rising from his
          Eastern couch, with his radiating flakes of fire, and is
          giving forth to the world another bright day. Let me pursue
          the recital of my woes burdened as I am with so many and such
          great misfortunes, and let me rehearse to thee my oft-repeated
          plaints, and let me surpass the Alcyons (Ceyx and Alcyon)
          which give out their dismal notes, as they hover over their
          aquatic abodes (during the nidifying season) and let me exceed
          too the Pandionian birds (Progne and Philomela) with my
          dolorous strains! for my troubles are greater, than ever
          theirs were—it is always a mother, [#]_ a mother that is the
          prominent theme in my lamentations, the first cause of my
          misfortunes, hear then the sad plaints of a daughter—if any
          sense or feeling is to be looked for in those numbered with
          the shades; I wish that Clotho had broken the threads of my
          life, with her venerable fingers, before, ever plunged in the
          abyss of grief, I beheld the wounds on thy body (Messalina’s)
          and thy face besmeared with the unsightly blood! Oh! this
          access of the light of day, it is always distressing to my
          mind (from the repulsive reminiscences). Light is now more
          odious to me, than ever Stygian darkness could be, ever since
          that sorrowful time—I have had to submit to the imperious
          tyranny of a step-mother, [#]_ her hostile spirit, and her
          savage glances! It is she, she that, like cruel Erinnys, has
          imported her Stygian torches and disturbed the harmony of the
          marriage homestead! And she has destroyed thee. oh! my father
          a thousand times to be pitied, whom till now, the whole world
          beyond the very ocean! owed subjection—at whose appearance on
          their shores, the affrighted Britons fled in dismay; having
          never before owed allegiance to any foreign conqueror! Ah me!
          oh! my father, thou art laid low, fallen by the wicked snares
          of a wife (destroyed by one of the fungi, Boletus, a poisonous
          mushroom) and thy palace and thy off-spring are under the
          cruel rule of a tyrant.


:OCTAVIA’S NURSE: (On account of the sad misfortunes befalling her
                     nurse-child Octavia, the nurse execrates the
                     drawbacks which beset the proud surroundings of
                     life in a Palace.) Anyone that is captivated at
                     first sight by the outside splendor and fleeting
                     advantages of the treacherous palace, can now
                     behold with his own eyes, wonderstruck, and realize
                     what remains of a once most powerful dynasty
                     overthrown on a sudden by the insiduous advantages
                     of adverse fate, and see what has befallen the
                     offspring of Claudius, to whose imperial sway the
                     whole world was once subject, and by whom that
                     Ocean hitherto free, and unnavigated over, was
                     brought under control and was constrained to afford
                     an unopposed passage for our Roman fleets! Think
                     that it was he who first placed the Britons under
                     any foreign yoke, and covered the very seas, before
                     unknown to the Romans, with his fleets, and amongst
                     even such barbarous nations, and such tempestuous
                     seas, he was, at all events, in a state of personal
                     safety! But alas! he fell at last by the wickedness
                     of a wife—presently she will share the same lot at
                     the hands of a son (Nero), and a brother of whom is
                     now lying dead from the effects of poison.
                     (Britannicus was not a brother, by the ties of
                     blood, Nero became a brother by adoption only.)
                     That miserable sister (by marriage only) and
                     likewise wife, is in a deep grief, nor does her
                     restrained anger suffice to conceal her terrible
                     woe—she always avoids being alone with her cruel
                     husband, eschewing privacy, and her angry
                     sentiments are quite on a par with the aversion
                     which the husband entertains towards her! They burn
                     with mutual hatred!  The confidence, which she
                     reposes in me, is in some sort a consolation to her
                     grieving heart, but devoted affection is quite
                     useless, in as much as her uncontrollable grief
                     thwarts all my well-intentioned advice, nor can her
                     resolute strong-mindedness, be in any way brought
                     under by my efforts, but she even seems to have
                     acquired increased determination, arising out of
                     the very misfortunes she has undergone! Alas! what
                     wicked crime do my alarms lead me on to foreshadow,
                     would that the kind intervention of the Gods may
                     avert such a climax!


:OCTAVIA—NURSE.: (The Nurse consoles the grieving Octavia, and dissuades
                   her from prosecuting any revenge, which she might be
                   contemplating)

:OCTAVIA: OH! my cruel destiny, to be equalled by none, in the severity
          of my misfortunes, it may be, Electra, [#]_ that I shall
          rehearse thy griefs in my own personal sufferings—it was thy
          fate to have to bewail the loss of a murdered parent, but in
          thy case, there was a brother in view, to revenge, at some
          future time, by that terrible crime a brother, whom thy
          affection snatched away from the sword of the enemy and to
          whom thy fidelity gave its sheltering protection: but my fear
          for the consequences hinders me from even outwardly bewailing
          the loss of my parents, who were snatched away from me, by the
          cruel hand of fate; it forbids me, too, to bemoan the death of
          a brother, in whom my one, my only hope was centred! There was
          a brief interval of consolation afforded me amidst such great
          misfortunes (while the brother Britannicus lived), but now,
          forsooth, I am handed over alone, with no brother to look
          forward to, to my own bitter grief, and thus I remain only,
          now, as the shadow of a once great name!

:NURSE: Alas! a sorrowing voice has struck my ears! and why should I,
        although affected with the tardiness of old age, hesitate to
        hasten with quickened steps to the bedchamber of Octavia?

:OCTAVIA: Trace these tears to their proper source, Nurse, thou art the
          one faithful witness of my grief.

:NURSE: What day will ever arrive, oh, thou one to be pitied, which will
        rid thee of thy troubles?

:OCTAVIA: What day (dost thou mean) will arrive? (Is it) the day on
          which I shall be packed off to the Stygian Shades?

:NURSE: I beseech the Gods, may such an unpropitious day as that, then,
        be a long way off!

:OCTAVIA: Unfortunately, thy wishes, Nurse, have no influence over such
          troubles as mine, but the Fates have!

:NURSE: Surely a merciful deity will vouchsafe better times for the one
        afflicted as thou art; but thou hast calmed thyself down
        somewhat, just try and prevail on thy husband’s
        susceptibilities, if he has any, and assume a bland, obsequious
        demeanour towards him.

:OCTAVIA: I shall have to overcome, first the savage lion of the plains,
          and the fierce tiger of the jungle, before I can subjugate the
          adamant heart of the tyrant Nero.—The fact is, he has an
          instinctive hatred to start with, of any one descended from an
          illustrious race—he despises alike, the ignoble herd of
          mankind and the Gods above as well, nor has he received
          anything at the hands of fortune, but what a cruel parent has
          heaped upon him, as the proceeds of aggravated crime; although
          he is ungrateful enough to be ashamed of ever having received
          anything from that cruel mother, he has, nevertheless, taken
          upon himself, the dominion over this empire, and although, in
          return for such a great gift, he hands her over to be
          assassinated! But a woman will long hold the credit for her
          share in the transaction, even after her death, and it will
          continue to last for many a long year in the minds of the
          people.

:NURSE: Restrain the expressions of thy angered mind, weigh with care
        the words thou sufferest to escape thy lips.

:OCTAVIA: Although I may patiently suffer these things, and appear to
          tolerate them, my misfortunes can never be brought to an end,
          but by the sad alternative means of Death! What with
          a murdered mother—a father snatched from me by a wicked
          crime—robbed of a brother—overwhelmed with all kinds of misery
          and grief—hateful in the eyes of a husband, and exposed to the
          insolent authority of a subject, [#]_ it cannot be supposed
          that I can enjoy my life vastly! My heart is perpetually in
          a kind of tremble, not from the fear of death, but from the
          possibility of some crime being committed! May I, however,
          never be fated to perpetrate one! It would please me to die,
          and the punishment of death itself could not be more dreadful
          to bear, especially by me in my miserable state, than having
          to encounter the angry and murderous looks of that tyrant
          (Nero) and then to have to exchange kisses with a downright
          enemy, which I know him to be, so as to dread his very nod!
          Whose caresses my inward grief could not permit me to
          entertain, and after that fate of my brother’s, who fell
          a victim to his crimes, and whose very empire he has usurped,
          and who glories in having been the author of that impious
          slaughter! How often is the tristful ghost of my brother
          brought before my mental vision, when a state of bodily repose
          relaxes my tired frame, and sleep invades the lids so wearied
          with weeping—Sometimes the ghost arms its feeble hands with
          funeral torches, and aims its blows at the eyes and face of
          his brother, (Nero was a brother by adoption only) who, in
          a state of alarm takes refuge in my couch—the enemy still
          pursuing him, and making a rush at him, as he is clinging to
          me, passes his sword through my side! Then the tremors come
          over me, and an intense dread drives away further sleep and my
          grief is renewed, and the alarms, as to my own miserable fate,
          return to me in force.—Then add to this—that insolent
          concubine (Poppaea) shining forth bedecked in all the finery
          which our palatial home affords her; to gratify whose whims
          and caprices, that son has caused his own mother to be
          embarked on board an unseaworthy craft, veritably only one
          meant to reach the Stygian banks! (that is, one which meant
          destruction, that would easily fall to pieces through the
          action of the waves, and be wrecked) and that mother whom,
          after the craft had become a wreck, and the difficulty of the
          waves had even been surmounted, he slew with his sword, and
          which proved to her a more cruel enemy than the waves of the
          sea! What prospect of safety dost thou think there can be, and
          security for me, after such a crime as that? That hostile
          woman, that Nero-conqueress, Poppaea is like some tempestuous
          cloud, hovering over my matrimonial bondage, and is burning
          with her hatred towards me, and she is now requiring at the
          hands of a husband, the life of a legitimate wife, as the
          price of her infamy! Oh my father, be thou emerged from the
          Stygian streams, and grant aid to thy daughter, or the earth
          being opened up, bring to my view that Stygian gulf, into
          which I would, myself, fain be borne headlong!

:NURSE: In vain thou invokest the Manes of thy father Oh! thou art much
        to be pitied—in vain I repeat, as amongst the manes, there is no
        anxiety with them, as to the offspring they left behind them
        (allusion is here implied to the Oblivion induced by Lethe), and
        he could prefer one of an alien race, to his own son, his own
        flesh and blood, and who took to himself by an incestuous
        marriage, a wife who was the daughter of a brother, has
        intermingled the race, by a most deplorable and unpropitious
        nuptial knot! Hence it is, that a whole series of crimes has
        been the outcome—murders—wholesale treacheries, the terrible
        grasping for power and that thirst for the cruel shedding of
        blood! The same day that the son-in-law of Claudius, Silanus,
        fell a victim, thy father’s marriage with Agrippina took place,
        lest he should be found to gather greater influence in
        consequence of thy marriage! Oh!  that intense piece of
        wickedness! Silanus [#]_ was presented to that vile woman,
        Agrippina, as a sort of wedding present and that noble young
        Roman stained with his blood his own paternal household gods,
        having been falsely accused, by a trumped up charge of
        fictitious crime! Woe is me! The arch-enemy has now entered the
        palace to which access has been gained by the treachery and
        wiles of a woman, and he that has been made a son-in-law of the
        Emperor Claudius, in the same way that he has been constituted
        a son by adoption, a young man of a most cruel disposition and
        capable of any crime, for whom that mother of his ignited the
        nuptial torches and joined thee by the marriage knot, although
        thou fearedst, and wast averse to such a union, and that
        ferocious woman, who accomplished whatever she set about, with
        great success, has actually dared to shed her imperious will
        over the cherished destinies of the very world! Who can describe
        the many forms in which crime has been served up, and the
        diabolical ambition of that woman, and her smooth, unsuspected
        treachery, whilst she is seeking to gain imperial power through
        every gradation of crime. Thus it is, that Piety with all its
        sacred associations quits the scene, in trembling horror! and
        thus cruel Erinnys, with all her ill-boding, advances into the
        palace to take her vacant place! She has defiled the sanctity of
        our household gods with her Stygian torches, in her fury, she
        has broken down the institutions of Nature herself, and set
        every human law at defiance—a cruel wife has prepared the
        poisoned bowl for a husband, and she, herself, has perished
        afterwards by the hands of a son—and thou also, Britannicus,
        hast been deprived of thy life, to be bewailed by us for ever!
        Oh! unhappy boy, till lately the great star of the Universe, the
        prop and mainstay of the Imperial Augustan Dynasty (the
        Caesars).  Oh! Britannicus! woe is me!  thou art now only
        a collection of flimsy ashes, and a tristful shade!  For whom,
        be it said, even thy cruel step-mother shed a few tears, when
        she gave up thy body to be consumed on the funeral pile,
        resembling as thou didst, the winged God himself, (Cupid) in thy
        shapely form and comely face—the greedy flames, however, took
        all that away! Octavia!

:OCTAVIA: And let them extinguish me in like manner, lest the tyrant
          fall by my hand.

:NURSE: Nature has not endowed thee, with such strength, as to enable
        thee to carry out such a threat.

:OCTAVIA: Long continued grief, anger, heaviness of heart, misery of
          soul, lamentations would supply me with the necessary strength
          I should think.

:NURSE: No! rather subdue that fierce man, by wheedlings and caresses.

:OCTAVIA: That I may induce him to restore to me a brother of whom he
          has deprived me by a cruel crime! Dost thou mean that?

:NURSE: No, not that; but that thou, thyself, might be in a state of
        security, that thou some day might build up the shattered
        dynasty, of which thy father was the dignified head, with thy
        own off-spring.

:OCTAVIA: The palace of the Emperor is expecting another arrival in the
          shape of offspring, the cruel fate of my miserable brother
          will soon drag me towards a similar end.

:NURSE: So favorable is the feeling of the citizens towards thee, that
        this fact goes far to conform my hopes.

:OCTAVIA: Yes! it is a good thing, to have one’s misfortunes pitied, but
          that does not remove nor even lessen the incubus resulting,
          therefrom—(the weight of troubles).

:NURSE: The power of the populace is great.

:OCTAVIA: That, however, of an Emperor is greater.

:NURSE: But he surely will have some regard for a wife.

:OCTAVIA: No! a concubine will stand in the way of that.

:NURSE: But it is granted, that she is odious in the sight of all the
        people.

:OCTAVIA: But she is held dear by Nero.

:NURSE: She is not a wife as yet, remember!

:OCTAVIA: But she will soon become one, and a mother as well!

:NURSE: Juvenile ardour, thou must remember, burns only as long as the
        early impressions operate, which called it forth, nor does it
        last long, ever, with these unlawful amours, it passes off like
        some flickering flame—on the other hand, the love of a chaste
        wife is an enduring possession—she is, as thou art aware, only
        the first who has ventured to violate the sanctity of thy
        marriage-bed, but this rival of thine, although a subject, has
        possessed the affections of thy husband for a long time—it is an
        old love affair—but this same woman is now evidently, more
        submissive and more subdued in her manner, as if she feared that
        some one else might be preferred to herself (lest in like
        manner, another may be preferred to herself as she, herself, was
        to Octavia), and she shows this by various indications, by
        which, as if tacitly confessing it, she openly portrays her
        fears! And the winged God (Cupid) may leave her in the lurch,
        let her beauty be never so transcendent, or however proud she
        may be of her wealth of physical attractions—all this sort of
        thing amounts to a very limited lease of human enjoyment. The
        Queen of the Gods herself, has, aforetime, undergone grief
        similar to thy own, when Jupiter, the lord of the heavens, and
        father of the Gods, changed himself into all kinds of shapes,
        and when, at one time, he assumed the plumage of a swan (to gain
        the better of Leda), at another time, he donned the horns of the
        Sidonian bull, (when he carried off Europa) then again, the same
        Jupiter has fallen upon another, as a golden shower (when he
        introduced himself to Danaë). The constellations of Leda are now
        shining in the heavens, Bacchus is duly installed in his
        father’s Olympian kingdom and Alcides possesses Hebe as a wife,
        now that he has been made a god, nor does Alcides any longer
        fear the anger of Juno, whose acknowledged son-in-law he is now,
        having married Hebe, but who was formerly considered in the
        light of an enemy! However, the wise submissiveness of an
        exalted wife like Juno, with her dissembled grief, has
        completely overcome the temper of Jupiter, and the mighty Juno
        reigns supreme in the ethereal marriage couch of the Thundering
        Jove!  Nor does Jupiter, now desert the palaces on high,
        captivated by mortal beauties; and thou, Octavia, art another
        Juno, although a terrestrial one, thou art the sister and wife
        of an Augustus. (The emperors at that time assumed the title of
        “Augustus.”) Conquer therefore thy troubles as Juno did.

:OCTAVIA: Let the stormy seas seek cordial companionship with the stars
          and let fire mingle with water, let the very heavens descend
          and take the place of grim Tartarus, let balmy light amicably
          join hands with hideous darkness, and bright clear day ally
          itself with the dewy night, before my mental tenderness could
          harmonize with the impious disposition of that wicked husband
          of mine. I am ever mindful of my murdered brother, I wish that
          the ruler of the heavenly gods would make ready to cut short
          with his lightnings, the terrible life of that cruel
          emperor—that deity, who so often shakes the earth with his
          frightful thunderbolts and terrifies our very souls with his
          awful igneous displays and novel wonders (fresh prodigies).
          But I have witnessed of late a blazing phenomenal splendor in
          the heavens, [#]_ a comet that has exposed to my view its
          ominous fiery torch, (tail) just where slow-moving Boötes,
          stiff as it were with the Arctic cold, drives his wagon at
          each turn of the night continually; behold, the very
          atmosphere seems polluted with the horrible breath of that
          cruel ruler.  The angry stars actually seem to be threatening
          the people with some fresh disasters, whom that impious
          potentate holds in domination. Not so bad was it, even, when
          the indignant earth formerly became a parent, and brought
          forth a ferocious Typhoeus, when Jupiter was not so much
          looked up to, as he is now—this present monster is worse than
          any Typhoeus ever was, for he is in addition, the avowed enemy
          of the gods and of mankind alike, for he has expelled all the
          deities from their temples—he has driven away the citizens
          from their native land, and robbed my brother of his life—he
          has drawn the life-blood of his own mother—and is he not still
          allowed to behold the light of heaven? and, moreover, does he
          not seem to enjoy his vile existence and drag on his noxious
          life? Alas! Oh!  thou supreme father of all, why dost thou,
          invincible as thou art, hurl thy lightnings, oftentimes, so
          harmlessly from thy regal hand? Why does thy hand hesitate, to
          hurl them with efficacy upon one so guilty as is Nero?—I wish
          that Nero could be made to pay the just penalty of his
          crimes—he (an adopted son of Dion Domitius, his adopting
          father) is the very tyrant of the universe, which he takes
          care to oppress with an ignominious yoke! he fairly
          contaminates and compromises the very name of Augustus, with
          his vicious tendencies and confirmed immoralities!

:NURSE: He is altogether unworthy, I am free to confess, of being
        married to a woman like thee, but is it not better, dost thou
        not think, to bow to the Fates (the inevitable) and to go on
        hoping for some favorable change on the part of fortune (chapter
        of events). My nurse-child, I beseech thee to ponder over all
        this and take it to heart and never excite the anger of thy
        violent husband—perhaps some avenging deity may crop up (exist)
        who will come to thy aid, and may that auspicious day arrive!

:OCTAVIA: Already our dynasty is under the ban of oppression through the
          severe anger of the Gods—first, when cruel Venus stepped in
          and impregnated my wretched mother with those lustful desires,
          who, ignoring us, her children (in a state of sexual madness,
          nymphomania) and though, already married, contracted an
          illicit matrimonial union with Silius (a sham marriage),
          thinking nothing at all about the husband she had already, and
          not troubling her head in the slightest degree, as to the
          lawlessness of such a proceeding. With her hideous locks,
          hanging loosely, duly surrounded with their serpents, that
          avenging Erinnys was present at this veritably Stygian
          marriage ceremony, and only extinguished the nuptial torches,
          to be seized upon for the purpose of future blood-shedding!
          For it inflamed the outraged breast of the Emperor, with such
          murderous wrath, as to culminate in the cruel slaughter of my
          mother, and thus my unfortunate parent fell a victim to the
          sword, and her death has overwhelmed me with never-ending
          grief! As the consequence of all this, she has dragged in her
          train, her husband and her son, to the shades below! And has
          handed over our dynasty to its downfall!

:NURSE: Do refrain from a renewal of thy grief, and of those tears,
        which I know thou only sheddest out of affection for the Manes
        of thy parent, who has undergone a heavy punishment for her mad
        conduct!


:CHORUS.: The Chorus being in favor of Octavia, looks with detestation
          upon the marriage of Poppaea, and condemns the degenerate
          patience of the Romans, as being unworthy, too indifferent and
          servile, and inveighs against the crimes of Nero.  WHAT report
          is this, that has just reached our ears—we wish that if such
          a story be wrongfully believed, although it may have been so
          industriously, canvassed abroad, and in such a purposeless
          manner, that it may not meet with any future credence—let not
          a fresh wife, usurp the marriage-bed of our empress! let the
          wife sprung from the loins of Claudius still reign supreme,
          over her own household gods! And may she, by a happy
          child-birth, bring forth those guarantees of peace, which the
          tranquil universe will hail with joy, and let Rome preserve
          its everlasting glory (among nations). The mighty Juno has
          drawn a prize in the lottery, of fortune, and now shares the
          couch of her husband, and brother, in absolute security and
          why should not the sister of Augustus, (that now is) having
          reconciled her matrimonial feud, do the same thing!  Why is
          she to be driven away from her paternal palace? If that is the
          case, what does her devoted piety (moral observances) profit
          her? What good has the having possessed Divus for a father
          done for her? What good has her virginity done her?  And what
          earthly use has her chaste modesty been to her? But we are all
          forgetful of what we once were, since the death of our
          emperor, whose race we are inclined to ignore in a manner,
          owing to our fear of that Tyrant Nero! Once upon a time, there
          did exist the Roman type of bravery amongst our ancestors, and
          the genuine progeny of Mars, and the true racial blood flowed
          in the veins of the men of bye-gone days! They drove out,
          without the smallest hesitation, haughty, insufferable kings
          from their cities! And they nobly avenged thy manes, oh!
          Virgin thou!  (Virginia) who wast slain by the hands of
          a parent, lest thou shouldst undergo an odious slavery, or
          that cruel lust should carry off victoriously its wicked
          prize! Sad war, too, followed on after thee, oh! thou daughter
          of Lucretius, so much, to be pitied, who was sacrificed by
          thine own hand, after having been ravished by a cruel tyrant
          (Sextus Tarquinius). At the hands of our outraged ancestors
          Tullia, the wife of Tarquinius, was punished for her cruel
          crimes—she who wickedly drove her cruel chariot over the body
          of her murdered father, and who, although a daughter, denied
          the accustomed funeral pile to the mutilated remains of the
          old man! Our own time, even, has witnessed an abominable
          crime, when the emperor, treacherously seizing upon the person
          of his parent, had her conveyed in a Stygian Craft (that is
          one meant for the purpose of destruction) across the
          Tyrrhenian Sea; the sailors receiving their orders, hastened
          to leave their tranquil harbours, and the waves soon resounded
          with the plash of their oars, and the craft shoving off, was
          quickly borne upon the sea, and which from the force of the
          waves soon springs a tremendous leak, letting in the sea, the
          hull giving way on account of the looseness of its timbers,
          and it ships a heavy sea! A great shout, thereupon is raised
          towards the sky, mixed with female cries, and cruel death, in
          various shapes, is now wandering before their eyes, each one
          seeks to escape from a watery grave—some in a state of nudity
          clung to the planks of the shattered craft, and with their
          aid, ply the waves successfully—others reach the shore by
          swimming—many are immerged, and hurry to their fate into
          a deep sea! Augusta (Agrippina) rends her garments, tears her
          hair, and deluges her face with her sad tears after a little.
          There is no prospect of safety, and burning with inward rage,
          and although fairly overpowered by the disaster, she exclaims:
          “Oh! my son, is this the reward, for the benefits I have
          lavished on thee? I am indeed worthy of having been caused to
          embark in such a craft, who have brought thee into the world
          and who have given thee thy very life, and in my motherly
          weakness have handed over to thee the proud name and empire of
          the Caesars! Oh! my husband, show thy face from out of the
          Acheron, and feast thy eyes on the punishment I am now
          undergoing—I, oh! thou to be pitied one, was the cause of thy
          death, and the instigatrix of the death of thy son
          (Britannicus) also! Behold! as I have richly deserved, let me,
          unburied, be borne off to join thy manes—let me be overwhelmed
          by the cruel waves of the sea” (at this moment the waves
          strike her in the face, as she is speaking) she plunges into
          the sea, sinks, but soon rises again to the surface, and
          impelled by her fear, she strikes out with her hands, but
          being soon tired out, gives up the struggle.—But a great deal
          of loyalty lurked in the silent hearts of the sturdy Roman
          sailors, this awful death being looked upon by them, at last,
          with excessive disgust! Many of the crew venture to render aid
          to their former empress, when they see that her strength is
          breaking down, and although they assist her with their hands,
          as she is feebly struggling with her own arms, and encourage
          her with kind words of sympathy, they remark: “What does it
          avail thee thus to have escaped the waves? thou art doomed to
          die by the sword of thy son, to which crime, distant
          posterity, although credulous as a rule, will scarcely lend
          their belief.”—He rages (Nero) and is angry that his mother
          has been rescued from the waves and is still alive; he then
          perpetrates a monstrous double crime! He madly rushes to
          effect the murder of his mother, and suffers no delay in the
          fulfilment of the crime: one of his followers is told off, and
          carries out his orders to the full! this fellow lays open with
          his sword the breast of Agrippina, and whilst she is dying,
          this unhappy mother, with her last breath, asks the
          perpetrator of her murder, to bury the cruel weapon into her
          very womb. “This is the place,” she says, “this is the spot
          that must be pierced with thy sword, the place which gave
          birth to that monster of a son!” After these words
          intermingled with much groaning, she surrendered her sad life,
          finally brought about by those cruel wounds!

ACT II.
-------

:SENECA: (THE philosopher despises the vices of his times, praises the
         simplicity of his former life, and gives it, as his opinion,
         that all things are tending in a direction for the worse.) WHY,
         oh powerful fortune, who hast been so alluring to me with
         deceptious outside show, hast thou summoned me from my former
         position, with which I was supremely contented? Is it, that
         from my being raised so high, I should fall all the more
         heavily, or that I might have a fuller prospect, from my
         elevated post, of the many dangers I might see around me? I was
         much better off, when I was hidden away at a distance, remote
         [#]_ from the perils of envy, amongst the rocky coasts of the
         Corsican sea, where my inclinations were unfettered and where
         I felt that I was my own master, and where an ample margin was
         afforded me for the following up of my favorite pursuits. Oh!
         how it used to delight me, to look at the glorious sun, than
         which, our first parent, nature, the artificer of that immense
         work, has produced nothing grander, and the awe-inspiring
         courses traced out by that solar luminary, to contemplate the
         revolutions of the heavenly bodies, and the alternate tracks of
         the sun (indicating day and night) and the planet Phoebe, that
         orb which the wandering stars surround, and far and wide, the
         resplendent ornament of the firmament. Now, verily the world
         has arrived at its last day, which, if not so, and it lives to
         be older, so much so as again to lapse into the condition of
         indescribable chaos, when the crash of the fallen heavens will
         overwhelm impious mankind, so that it may for the second time,
         create a new race, and the one, that is to be born again, to be
         an improvement upon the present one—as it was, indeed, at its
         earlier periods, when Saturn held the dominion of the skies
         (the golden age). Then it was the Virgin Justitia (Astraea)
         that goddess of such distinguished reputation amongst the
         deities commissioned from Heaven, with that sacred trust, ruled
         the earth with mildness—The human race had never known what
         wars were, nor had they ever heard the battle-inspiring blasts
         of the shrill war-trumpet! and the people of those days were
         unacquainted with the weapons used in battle—they did not
         surround their cities with walls—the land was one grand
         highway, open to all; and the enjoyment of all things was
         within the reach of and common to everyone—and the smiling
         earth freely disclosed its fruitful bosom, and this Parent was
         happy in having the protection of such contented
         children.—Another age (the silver age) supervened, but the race
         of mankind was considered inoffensive, and the third (the
         brazen age) produced a skilled progeny—one that applied itself
         to new inventions, but yet was quite observant of the
         sacredness of the laws! by and bye, men became restless (the
         fourth race) and ventured to hunt the savage wild beasts, to
         draw out from the sea, in a net, the large fishes, which had
         hitherto been unmolested and protected by the waves, or to take
         the birds of the air aback, with their swift arrows, to bring
         into subjection the fierce bulls, and submit their necks to the
         yoke—to plough the earth, before free from the wounds of the
         ploughshare, which, however, when thus torn up, was found to
         hide away its productiveness, much deeper down in the bosom of
         its sacred interior (sacred because it had never been intruded
         upon). But this discontented age penetrated into the very
         bowels of its parent, and out of it, soon showing themselves,
         came the dreadful sword (iron) and gold (that incentive to
         crime), and very soon, mankind carried weapons of destruction,
         in their cruel hands! They parcelled out kingdoms, and defined
         the limits of territorial holdings, and built new
         cities—sometimes they defended the homesteads of others, used
         those weapons, threateningly, with plunder, only, for their
         object! Astraea, the bright ornament of the starry firmament,
         finding herself no longer held in respect or veneration, fled
         the earth, and avoided their savage ways, and looked with
         abhorrence at the hands of mankind stained with the blood,
         which flowed from their savage slaughters—and the thirst for
         gold likewise—and then came into view, the greatest evil of
         all, and spread throughout the world.—Luxury, that insidious
         curse of mankind, the long-continued indulgence in which
         involving such a pernicious departure from the lines of
         moderation, acquired additional power over mankind, as it
         became more confirmed, and the aggregate vices accumulating
         throughout so many ages, have been very abundantly shown
         amongst us for a long time now—we are oppressed by very
         distressing times an age, in which crime seems to rule
         paramount, and rampant wickedness seems to take cruelty as its
         guide, whilst irrepressible debauchery is presided over by that
         salacious Goddess, Venus! Luxury, that successful conqueror,
         some while since, has grasped, with its greedy hands, the
         immense resources of the world (riches) so that they may be
         only squanderingly got rid of! But, behold, Nero is approaching
         with a step suggestive of something out of the usual way, by
         his truculent look—I quite shudder in my very soul, as to what
         is uppermost in his mind!


:NERO—PREFECT—SENECA.: (The philosopher warns his patron Nero to no
                           purpose, who pertinaciously insists on
                           carrying out his tyrannical plans, and
                           appoints the next day for his marriage with
                           Poppaea.)

:NERO: CARRY my orders out exactly, despatch some one, who will bring
       me, as soon as they have been cut off, the heads of Plautus and
       Sulla. [#]_

:PREFECT: I will not delay the execution of thy commands, I will
          forthwith repair to the camp.

:SENECA: It is wiser for thee to determine nothing rashly, especially
         towards friends, and those, allied to thy cause.

:NERO: It is easy to preach that doctrine to a man who himself is
       credited with justice, and does not suspect others, about whom,
       in short, his mind is free from apprehension.

:SENECA: Clemency is the most powerful remedy, in counteracting any
         danger arising from others.

:NERO: To stamp out an enemy, is the highest triumph an Emperor could
       wish for.

:SENECA: To look to the welfare of the citizens, constitutes the
         greatest virtue, in the father of a country.

:NERO: It is quite in keeping, that an old man should be mild, when he
       is laying down precepts for youngsters.

:SENECA: The ardor of the adult youth, on the other hand, requires more
         governing than that of mere boyhood.

:NERO: I think, that at my age, my own will is all that is necessary.

:SENECA: So long as the Gods above, may always approve of thy acts.

:NERO: It would be in a very silly superstitious way, that I should fear
       the Gods, when I am about to do anything!

:SENECA: Fear all the more, as to what would be considered right for
         thee to do.

:NERO: My good fortune (position) permits all things I may wish to do.

:SENECA: Be careful, as to the confidence, thou reposest in that fickle
         deity, Fortune, she is a very frivolous Goddess!

:NERO: He must be a dullard indeed, who does not know, what to permit
       himself to do.

:SENECA: It is a praiseworthy thing to do what is right, but the
         reverse, when it is not so.

:NERO: The common herd of mankind are inclined to spurn a man who is
       kind, gentle, and of whom they can take advantage.

:SENECA: They will seek to punish, though, one that is an object of
         hatred to them.

:NERO: The sword is the protection of an Emperor.

:SENECA: But it is a safer kind of protection that he should be beloved.

:NERO: It is proper that they should fear a Cesar.

:SENECA: But it is better that a Caesar should be loved.

:NERO: But it is also indispensable that they should fear.

:SENECA: Whatever is extorted from a man is sometimes an irksome gain to
         him, who obtains a thing by such means.

:NERO: But they must obey my commands.

:SENECA: That is all the greater reason that thy commands should be
         tempered with justice.

:NERO: I shall myself always determine, (what is, and what is not to be
       done).

:SENECA: But which, it is to be presumed, will obtain a favorable
         reception from thy subjects.

:NERO: The drawn sword, the employment of which some affect to despise,
       will do all that.

:SENECA: I pray thee, may such wickedness be absent from everything,
         thou mayest ever do.

:NERO: Shall I suffer anything more than that, as an unrevenged emperor?
       that my very blood should be regarded with contempt, and that
       I should be fallen upon unawares. Simple exile, I perceive, has
       not subdued the turbulent natures of Plautus and Sulla, though
       they have been removed to a long distance off—they, whose
       persistent madness is now arming the willing instruments of crime
       (assassins) with the view to my destruction! Considering also,
       that a large amount of sympathy towards the conspirators, whom
       I have exiled, still prevails amongst the people in this city,
       and who, no doubt, would further the aspirations of those exiles
       by every means in their power—my enemies, therefore, and those,
       I suspect to be such, must be removed by the sword—that odious
       wife of mine must perish,—she must follow that darling brother of
       hers; in short, whatever else is of lofty rank (and derives
       prestige from it) must fall!

:SENECA: Oh! it is an admirable thing to shine conspicuously amongst the
         illustrious men of the land, to consult the welfare of one’s
         country, to spare those that are afflicted, to abstain from
         cruel slaughter, to control one’s anger (to give time for it to
         cool down), to secure tranquillity for the world, peace to the
         age in which we live—this is the highest form of virtue, and by
         such a road is heaven only to be arrived at. It was in such
         a way, that the first Augustus (Octavius), the great parent of
         his country, was enabled to reach the stars, and he is
         worshipped now as a very god in the temples. Fortune, however,
         tossed him about both by sea and land, through many trying
         vicissitudes of war, as long as ever he contended against the
         enemies of his father, (Julius Caesar, who adopted Octavius).
         But the goddess, Fortune, without any shedding of blood, has
         showered her favors upon thee, has given thee government of
         a mighty empire, that thou mightst rule it without any
         difficulty, and has subjected the Earth and the Sea to thy very
         nod!  Contemptible envy has stepped aside, abased and
         overpowered by the devoted acclamations, which have been poured
         forth—the enthusiastic support of the Senate, and the
         equestrian order has been accorded thee, and it is by the
         unanimous vote of the people, ratified by the decrees of the
         senators, that thou hast been chosen as the fountain-head of
         peace, and the chief ruler of the human race; thou as a parent
         to thy country, governest the world in thy quasi-divine person
         Rome expects thee to cherish this honoured reputation, and thus
         freely hands over her citizens to thy safe keeping.

:NERO: It is a gift of the Gods, no doubt, that Rome and the Senate
       should be subservient to my authority, forasmuch as it is only
       the fear they entertain of me, which draws from their reluctant
       lips, those cringing supplications, and the low-toned fawning
       voices which mask all this affected humility. But that the
       factious citizens, conspirators against their country, and my
       person as Emperor, puffed up with pride, about their illustrious
       descent, should pretend to serve me willingly! What downright
       madness it would be, to entertain such a wild notion! But at the
       same time, it is competent for me, an Emperor, with one word to
       consign anyone, that I might suspect of criminal designs, to
       immediate death! Brutus armed his hands for the slaughter of his
       generalissimo (Julius Caesar) from whom he had received every
       marked friendship, and support. And that great Caesar, who had
       never been vanquished in battle, the conqueror of so many
       nations, oftentimes was regarded, as the equal of Jupiter
       himself, judging from the elevated pinnacle, to which his honors
       had raised him, in the eyes of the people, (Jupiter ruled all
       things in heaven, Caesar, all things on earth) fell by the crimes
       of the citizens! How much blood did Rome, torn by the intestine
       factions of its citizens, see shed by such internecine slaughter!
       Divus Augustus, who won his way to Heaven, by those praiseworthy
       deeds of valor of his: how many nobles, young men, and old men,
       had he slain, scattered as they were, over the world, when they
       deserted their very homesteads, with the fear of death staring
       them in the face, and fled from the swords of the triumvirs,
       shuddering as they cast their eyes at the proscription tables,
       which registered the names of those that were doomed to death!
       and the grieving senators saw the heads of the slain, exposed for
       inspection in their very Rostra, (a place in the Senate, Rostrum)
       nor was it allowable for anyone to weep for the loss of those who
       had belonged to them, nor to sigh even, when the forum became
       positively infectious, through that dreadful slaughter, the
       sanious filthy discharges still dripping from their decomposing
       faces; nor did this blood-and-slaughter business stop here, by
       any means—the cruel birds of prey, and wild animals feasted for
       many a day on the mortal remains which lay exposed [#]_
       (unburied) on the plains of Philippi, and the Sicilian sea drew
       their ships into its watery gulf, and the crews, which had been
       worsted in this fratricidal fray, by men of their own blood, and
       the bulk of the people, were fairly shattered by the warlike
       persistency of the combatants! But Antony, being worsted in
       a battle, was obliged to make for the Nile, in the ships already
       prepared for flight,—he himself being doomed to perish, shortly
       after—and thus, incestuous Egypt, (on account of the marriage of
       Cleopatra with her brother Ptolemy) again imbibed the blood of
       a Roman general, and now it covers up his insignificant remains!
       Then, indeed, was the civil war, which lasted so long, brought to
       an end, and then at last, the tired conqueror sheathed his
       truculent sword, absolutely rendered blunt by the many terrible
       blows it had inflicted, and he continued to rule, but it was
       through the fear he had inspired!  He was safe then, with his
       armaments, and the fidelity of his soldiery.—Here, then, was that
       Deity, who was made great by the devoted services of a son
       (Tiberius), canonized after death and handed down for adoration
       in the temples. And in a similar manner, the stars will hold good
       for my reception, if I am prompt with the stern sword, and employ
       it against everything that is hostile to my interests! and
       I myself shall have laid the foundation-stone of a future
       dynasty, for some offspring equally worthy!

:SENECA: That glorious ornament of the race of Claudius, will yet live
         to fill the palace with the celestial stock, descended from
         a Divus, (by Octavia is here meant) after the example set by
         Juno, sharing the nuptial-bed of her brother (having buried
         past differences).

:NERO: An incestuous mother-in-law (Messalina) is rather apt to shake
       confidence out of a son-in-law, and what is more, the disposition
       of this wife of mine, has never harmonized with my own.

:SENECA: During the tender years of a young woman’s life, her confiding
         love is not sufficiently shown, she is then so much under the
         dominion of bashfulness, that she conceals from observation,
         the amorous fires which lurk beneath that shyness.

:NERO: Indeed! I have clung to that notion in vain, for a long time too!
    and altogether it is self-evident to me, from her unsociable tone, and
    manner, the symptoms of absolute hatred towards me, are obvious
    enough in her very look—so much so, that my burning indignation has
    determined me to take my revenge, and with that end, I have found
    a wife worthy of my marriage-bed, both as regards her birth and her
    unequalled beauty, a woman to whom Venus herself would yield the
    palm, or even the wife of Jupiter, or that other goddess, so fierce
    in battle (Minerva).

:SENECA: Probity, faithfulness in a wife, strict morality, and modest
         reserve should be, what ought to please a husband—those lasting
         advantages of mind, and heart, second to none in importance,
         are those and those only which continue permanent, and as long
         as life lasts; but thou oughtest to know that each day steals
         away a portion of the beauty of every flower.

:NERO: A kind deity has moulded all these gifts in one individual,
       Poppaea; thou perceivest that the kind Fates have actually willed
       that such a one (impersonating all these qualifications) should
       have been born expressly for me.

:SENECA: Let all thoughts of love be banished from the mind at once,
         lest in some rash foolish moment, thou mightest believe all
         this sort of thing to be a downright reality!

:NERO: Dost thou mean that little deity, whom the God of Lightning, and
       the grand ruler of the heavens, is unable to drive away from
       himself, who penetrates the recesses of the angry sea, the
       kingdom of Pluto, and draws down from their celestial abodes, the
       very Gods above?

:SENECA: It is a mistake, we mortals commit, when we picture the winged
         god Cupid as a cruel deity; we arm his hands with arrows, and
         add to them the fatal bow and the cruel torch, and delude
         ourselves that he was born from Venus and sprung from the loins
         of Vulcan—the fact is, Love is a potent force springing from
         the imagination, and an insinuating passion, which rises up in
         the human breast; it begins to show itself in youth, and is
         kept alive by luxurious surroundings, want of occupation amid
         the alluring advantages held out by fortune, the which, if thou
         failest to cherish, and pamper, soon languishes, and being thus
         deprived of what preserves its existence, loses its influence
         in a short time!

:NERO: I am of opinion that this passion is the principal object in
       life, by whose influence, pleasure accrues to its votaries, for
       as much, too, as the human race will always continue to be
       reproduced by this agreeable means, (Love) it is that likewise,
       which has the power of mollifying the fierceness of the wild
       beasts. At all events, this little deity shall lead the way, with
       his marriage torches, and shall yoke Poppaea to my nuptial couch
       with his seductive fires!

:SENECA: The indignation of the populace will scarcely tolerate being
         the witnesses of this marriage, nor will the solemn ordinances
         of piety sanction it.

:NERO: Shall I be the only one to be prevented from divorcing a wife,
       a privilege which is allowed to every one.

:SENECA: The people exact higher and nobler observances from him who is
         the acknowledged head over all men.

:NERO: It will please me to try, and, moreover, whether that foolish
       partiality for Octavia, which has crept into the noddles of the
       Romans, shall not give way, when it is beaten out of them, by my
       weight and authority.

:SENECA: Rather comply placidly with the wishes of the citizens.

:NERO: It must be, indeed, a sorry departure from the methods of
       governing, when the vulgar herd dictate terms to an emperor.

:SENECA: That man only has a right to complain, who can obtain nothing
         whatever, that he seeks, to be granted him.

:NERO: It is quite right then, to enforce a thing to be granted, which
       solicitations fail to obtain?

:SENECA: It is hard to have to deny anything to a suppliant.

:NERO: But it is a crime, I should think, to attempt, even to coerce an
       emperor.

:SENECA: But that emperor should relax his desires sometimes.

:NERO: But then the report would get about,—Oh! we have brought the
       emperor to his senses, thou seest! (that the emperor was beaten).

:SENECA: Such a report as that, would be silly, and exercise no effect
         on anyone.

:NERO: But it might be that such a notion would strike the minds of
       many.

:SENECA: As a rule, the public approach matters, above their own level,
         with some degree of diffidence.

:NERO: They might not censure the less, however.

:SENECA: But that could easily be put down. Will not the tender age of
         thy wife, her probity, her modesty have any effect in breaking
         through thy objections, to say nothing of the great benefits
         which thou hast received at the hands of her father Divus?

:NERO: Do cease, for the last time, urging thy objections—it is really
       too much for me to listen to; it is in my power to do what Seneca
       condemns, and I myself am only biding my time for the
       acquiescence of the people, when Poppaea shall carry in her
       uterus some pledge of my affection, and a representative part of
       my ownself!  Therefore I fix the earliest day for my marriage,
       namely to-morrow!

ACT III.
--------

:AGRIPPINA: (AGRIPPINA appears from the infernal regions, a cruel
            soothsayer carrying before her the fatal torches, at the
            nuptials of Poppaea, and Nero whose death she
            predicts.—Shade of Agrippina speaks.) THE Earth being
            opened, I have found my way out of Tartarus, bringing in my
            unrelenting hand, the Stygian torches to grace this wicked
            marriage.  It is with these torches, Poppaea shall be joined
            in marriage to my son, which the avenging hand, and
            indignation of an outraged mother, would rather employ for
            a graver occasion, his funeral pile (Nero’s). May the memory
            of my impious slaughter cling to me, as long as I am
            numbered with the shades, oppressed with the thought, as
            I am, that these hands of mine have gone unavenged, and the
            fatal craft intended for my destruction, given to me, as the
            reward for my services, and that dreadful night, which he
            has given me as the price of the Empire I gave up to him, on
            which I had to bewail my shipwreck—it had not been an object
            of my desire on my part, to have duly bewailed the deaths of
            my companions in misery (Creperius, Gallus and Aceronia),
            the results of the cruel crime of a son, but no time was
            afforded me, even for shedding tears—for Nero coupled his
            previous wickedness with another crime, and being slain by
            the sword, I yielded up my burdened existence, within the
            proximity of my venerated household gods, nor I, even then,
            stifled the persecuting hatred of that son of mine, with my
            last drops of blood—the cruel tyrant began to grow wrathful
            against the very name and memory of his mother, his desire
            was, that any claim to merit on my part should be completely
            effaced—he caused to be destroyed all pictured likenesses or
            sculptured models, and all inscriptions which represented
            me, on pain of death, throughout the whole world, the Empire
            of which I, in my foolish love, gave to him, and all this,
            too, that as a requital, he should eventually take away my
            life! But my husband Claudius, who was cruelly deprived of
            life, disturbs my very manes; he rushes with his torch at my
            face, which is hateful to him to behold, he is ubiquitous in
            his presence, he menaces, and imputes to me his own fate and
            the death of his son Britannicus, and demands to know who
            was the actual murderer (Nero).  Spare me, Claudius, thy
            reproaches; he shall be given up, and I ask no long time
            either for it to be brought. The avenging Erinnys is
            preparing a condign death for such a cruel tyrant—she is
            making ready to inflict the stripes and pave the way for the
            ignominious flight, and the punishment with which a Tantalus
            is to quench his thirst, and for the cruel task of
            a Sisyphus, and the rapacious vulture of a Tityus, as well
            as the wheel, which whirls round rapidly the body of an
            Ixion! He may, indeed, erect his marble monuments, and in
            his pride, gild the very roofs of his palace, and the armed
            trained bands (cohort) may vigilantly guard the portals of
            their emperor and the thresholds of his palace, the very
            world may, through his exactions, be drained of its riches
            to answer to his beck and call! The Parthians in suppliant
            humility, may seek to salute with the kiss of submission,
            that sanguinary right hand of his, and Tiridates may throw
            his kingdom, and all the riches he possesses at the feet of
            Nero! But the day and hour will arrive soon on which he
            shall give up that criminal life of his; for the wickedness
            of which he has been the author, his throat shall be a very
            target for the javelin of the enemy, he shall be universally
            shunned, ruined, and reduced to absolute want! Alas! how all
            my labor—how my fondest wishes have turned out! Oh! thou son
            of mine, whither has thy madness drifted thee, and to what
            a fatal destination! The just anger of thy mother, who fell
            by thy crime, is a paltry consideration, compared with the
            many punishments thou wilt have to undergo! I wish, though,
            before I had ever brought thee into the world, as a little
            baby boy, and suckled thee at my breasts, that some
            ferocious wild beast had torn the very womb out of my body,
            or that thou hast died as my innocent suckling, without any
            knowledge of what existence was and without any crime to
            answer for!  joined to, and still leaning on me, thou
            mightst always have before thy eyes a quiet resting-place in
            the regions below, where thou mightst see around thee, thy
            father, thy great grandfather and men of our lineage of
            glorious reputation! Before whom, alas! there remain instead
            only disgrace and perpetual sorrow! and all this arising out
            of thy crimes, and myself, who have brought such a monster
            into the world. But why do I stay longer, why do I cease
            from hiding my face in Tartarus, the cruel step-mother of
            a Britannicus, the wife of a murdered Claudius, and the
            unfortunate mother of a Nero!

:OCTAVIA—CHORUS.: (Octavia, feigning sadness, prays the populace, who
                    are espousing her cause, not to grieve about her
                    divorce. The chorus, however, does grieve for her
                    sad lot.)

:OCTAVIA: SPARE these tears, on a day of such rejoicing and gladness to
          the city—let not the great affection thou hast for me, and the
          interest shown in my cause, rouse any feelings of bitter
          resentment in the heart of the Emperor, I may yet be the means
          of bringing great misfortunes upon thee—it is not the first
          time my breast has felt wounds like this—I have already put up
          with more grievous ones! May this day procure for me an end to
          my troubles, even if it be by death! There is one thing,
          I shall no longer be called upon to rest my eyes on the visage
          of my cruel husband, I shall henceforth be the sister and not
          the wife of an Augustus, and thus not be compelled to share
          the odious nuptial couch, with a rival! But I do pray, that
          sad mental tortures may be spared me—the apprehensions of
          crime and the fear of some cruel death! But! oh!  miserable!
          oh! demented Octavia, canst thou reasonably hope for such
          things, mindful as thou must be of the former crimes of this
          detestable man, or that he, who is accustomed to spare nobody,
          would deal gently and mercifully with thee? For a long time
          hast thou been reserved for such a marriage as this (to occur
          before thy eyes), and at last, as a sorrowful victim thou wilt
          fall; but why in that confused kind of way dost thou glance
          back upon thy paternal household gods, with such tearful eyes?
          hasten away rather from under such a roof, and quit for ever
          the palace of the blood-thirsty Emperor.

:CHOR: Behold!  the day shines forth at last, so long, and so much
       mingled with certain misgivings, yet so often canvassed abroad as
       mere hearsay.  Claudia has been banished from the nuptial bed of
       cruel Nero, and has surrendered the couch, of which the
       triumphant Poppaea, by this time, is the tenant in possession,
       whilst the affections we all felt for her, must now be put a stop
       to. Kept down by the terrible fear of consequences, and our
       indignation must be outwardly suppressed. But where is the
       ancient courage of the Roman populace, which often caused the
       most illustrious of men to fly for their lives? (Syphax, Perses,
       Jugurtha, Herodes) that populace which gave law and institutes to
       a country, which has never been conquered, and which, in ancient
       days, bestowed the magisterial dignities only on those who were
       worthy recipients—that populace decided, when there was to be
       war—and decided likewise when there was to be peace—they brought
       the turbulent nations into subjection, they confined conspiring
       captive kings in the prison dungeons! Behold, grievous as is the
       sight, on all sides, model images of Poppaea, dazzling our vision
       side by side with those of Nero! Let us dash to the earth with
       our violent hands those images which are only too like, the face
       of this newly created Empress! And let us drag her from her
       exalted couch, without delay let us, in our disgust, make for the
       palace of the cruel Emperor, with the fiery torch, and the sword
       of vengeance!

ACT IV.
-------

:NURSE—POPPAEA: (Poppaea, being frightened, in her sleep, narrates her
                  dream to the Nurse; the Nurse treating the dream as
                  nonsense, consoles Poppaea, with some silly
                  interpretation.)

:NURSE: HOW is it, my nursling, that thou art quitting the marriage
        couch of thy husband in such a state of terror, and of what
        hiding-place art thou in quest, with so troubled a countenance,
        and why are thy cheeks so wet with weeping? Surely, this day,
        which has been so long, and so anxiously looked forward to, has
        shone brightly in response to thy prayers and desires! Thou art
        matrimonially linked with a Caesar! The chief of the deities,
        Venus, and the Mother of Love, has given Nero to thee, bound by
        the sacred nuptial chains, and to one whom thy beauty has
        captivated, in spite of Seneca’s objections, [#]_ too, to such
        a marriage union! Oh!  what an important personage thou hast
        become, and in what a magnificent palace thou hast settled down,
        and upon what an exalted couch wilt thou now recline! The senate
        were fairly astounded when they beheld thy transcendent beauty,
        admired thee when thou offeredst up (with such reverence) the
        frankincense to the Gods, and when thou sprinkledst the sacred
        altars with the gladsome wine! the upper part of thy head, so
        gracefully shaded by the red veil (worn by recent brides, as
        tokens of modesty, and wifely subjection), and Nero, walking
        forth, amidst the enthusiastic acclamations of the citizens,
        holding himself up so loftily, and hanging on so closely to thy
        side! An Emperor all over, testifying with joy in his very
        carriage and countenance! Such, indeed, as Peleus manifested,
        when he took Thetis to wife, as she emerged from the foaming
        waves, whose marriage the Gods are said to have celebrated with
        great pomp and with the universal acquiescence of every deity of
        the sea likewise. But what hidden event has thus changed thy
        wonted expression of countenance? tell me why this paleness?
        What trouble do those tears indicate?

:POPPAEA: Oh! Nurse, suffering sadly from my harrowing thoughts, I seem
          to have utterly lost my senses: the fact is, I was perplexed
          and terrified by the doleful visions of last night, for when
          the expiring brightness of glorious day had given place to
          those gloomy stars, and the sky was handed over to the dark
          realms of night, I went off to sleep, hugged by the embracing
          arms of my Nero, but I was not permitted to enjoy my placid
          repose long—a lugubrious multitude appeared before me, as if
          to celebrate my marriage, and the Roman Matrons, with their
          locks loose and hanging down, gave forth the most distressing
          wailings, and amidst, every now and then, a terrific blowing
          of trumpets, and the mother of my husband (Agrippina) with
          a savage threatening look, flourished her torch at me, all
          covered with blood, whom, whilst I was following,—which I felt
          forced to do, so inspired was I with the fear which had taken
          possession of me—the earth seemed to be suddenly divided, and
          an immense yawning gulf lay open before me, into which opening
          I seemed to have been borne away headlong. I could perceive,
          at the same moment, and I wondered equally at this, my own
          marriage couch, the couch on which, I have before lain down,
          thoroughly fagged out—I then beheld him who was my former
          husband, Crispinus, advancing towards me, with a crowd
          following him, and then amongst them, my son (Rufus who was
          ordered, to be drowned by Nero). Crispinus rushes forward to
          seek my embrace and showered my face with those kisses which
          have been now so long in abeyance! when, all on a sudden, Nero
          breaks into my chamber, and buries his cruel sword deep down
          in his throat! (that of Crispinus.) At length, this excessive
          alarm, effectually chased away all further disposition for
          sleep! The horrible tremor into which I was thrown, has made
          my limbs tremble all over, and has impeded my very powers of
          utterance—and my heart palpitates to that degree, that it
          beats forcibly against the walls of my chest. My fear prevents
          me from expressing in words, what I feel, but thy fidelity and
          affection, Nurse, reassures me, and has given me back my
          powers of speech—Alas! Why do the Ghosts, from those infernal
          regions, think proper to molest me, and at the same time,
          might I ask what it was, when I distinctly perceived the blood
          of my husband?

:NURSE: Whatever subjects the mind is intent upon, or troubled about
        during our waking moments, such is the rapidity, and
        wonderfulness of human thought, altogether as it is a divine and
        mysterious property of the mind, that it reproduces, during
        sleep, those very things impressed on us during the day, under
        a variety of visions, and fantastic appearances. Thou wonderest,
        no doubt, that thou sawest a husband, a marriage couch, and what
        thou tookest for a funeral pile, whilst thou wert being embraced
        and hugged, by the new husband, but the breasts thou sawest
        being beaten in the dream, and the shattered locks, arose out of
        the excitement created by the auspicious event (the marriage
        day). The partisans of Octavia, were bewailing her divorce
        before the cherished household gods of thy brothers and thy
        paternal lares—that torch, which thou followedst, was carried in
        front of thee, by the hands of Augusta (Agrippina) and the envy
        aroused, by the marriage, foreshadow thy name as rendered still
        more illustrious thereby—the position in which thou wast placed
        in the Infernal Regions during thy dream, clearly indicates that
        the future marriages, in the durable dynasty, will henceforward
        be permanent in their tenure—then, as regards why thy Emperor
        husband thrust his sword into the throat of the spectre shows
        that he will never more excite wars, but that he means to hide
        it, henceforth in the sheath (the throat of the spectre only) as
        a guarantee of peace! Now collect thy scattered faculties, take
        on a cheerful look, I beseech thee, and shaking off all these
        fabrics of thy vision (fears having no foundation) betake
        thyself to thy bed chamber.

:POPPAEA: I had made up my mind to seek the temples and the sacred
          altars, and to sacrifice to the worship of the Deities with
          slaughtered victims, that such threatening visitations of the
          night, and the period allotted to sleep might be expiated, and
          that the terror inspired thereby, might recoil upon my
          enemies; and, Nurse, offer up thy prayers for me, and worship
          the gods above, with thy pious supplications, that the
          apprehensions which still hang about my mind, may pass away
          from me!


:CHORUS.: (The Chorus praises the beauty of Poppaea.) IF garrulous
          report tells the truth when it talks of the furtive amours of
          the Thunderer, and the love affairs in which he so much
          delighted; once, whom they report as having coaxingly embraced
          the bosom of Leda, whilst disguising himself with the wings,
          and feathers of a swan—at another time, transforming himself
          into a fierce bull, carrying off Europa, as a captive across
          the sea—even, now, Poppaea, Jupiter would quit the heavens
          above, and the starry firmament, which he is ruling, and seek
          the pleasure of thy embraces, and which he could, with reason
          prefer to Leda’s and even thine, Danaë, whom he admired so
          much and descended with amorous intent, in that yellow golden
          shower.—Sparta may brag of the beauty of that famous offspring
          of hers, Helen, and it is permissible enough that the Phrygian
          shepherd (Paris) should have been proud of his conquest! She,
          Poppaea will outstrip in beauty this daughter of Tyndarus, and
          who brought about dreadful war, and levelled the Phrygian
          Kingdom to the very ground. But who is this rushing on at
          a pace accelerated by some fright, or what news is he
          bringing, with his breath panting like that?—(out of breath).

:MESSENGER—CHORUS.: (The Messenger describes the excitement of the
                      populace, on account of the divorce of Octavia,
                      and this marriage with Poppaea.)

:MESSENGER: WHOEVER that soldier may be, who entertains a boastful pride
            in being a chosen guardian of the emperor’s portals, let him
            rouse himself, for the defence of the palace, which the fury
            of the populace is now menacing.—Behold, the Prefects, in
            a state of trepidation, are calling together (mustering) the
            armed bands, to garrison the city with extra protection,—nor
            does this insane feeling, which has so rashly sprung up,
            appear amenable to any kind of fear, but is acquiring
            greater and greater intensity.

:CHORUS: What mad fury is it, that is now agitating the minds of the
         populace?

:MESSENGER: This multitude of people are seized with rage about this
            treatment of Octavia, and being wild with anger, they are
            rushing on into every kind of crime.

:CHORUS: Tell us what they have had the audacity to do, and at whose
         instigation all this has originated.

:MESSENGER: They are making preparations to restore Claudia (Octavia) to
            the household of the Divus, the restitution of conjugal
            rights by her husband and brother, and her legitimate share
            of the imperial dignity.

:CHORUS: Of which, already, Poppaea is in full possession, through the
         legal marriage contracted by unanimous authority in good faith,
         and upheld by one-minded approval.

:MESSENGER: This excessive uncontrollable fury springs out of the
            indignation, to which these nuptials have given rise, and it
            is that, which is urging them on with headlong rashness,
            into this display of madness. Whatever statue of Poppaea,
            sculptured out of the purest marble stood in their way, or
            whatever brazen monument was shining forth and revealed the
            likeness of Poppaea, was ruthlessly dashed to the ground by
            the infuriated hands of the populace, and lies there broken
            up, by means of hammers wielded by savage arms; they then
            dragged the pieces of the statues, which had been pulled
            down from their standing place, trailed them along the
            streets, with cords, and after kicking them about for some
            time in an angry fashion, they would plaster them all over
            with fillthy mud! And the swearing, and cursing, that went
            on, and their obscene language was quite in keeping with
            their acts, and which was so bad that I should be afraid to
            repeat it; they are, now preparing to surround, the Palace
            with flames, unless Nero surrenders this new wife of his, to
            appease their indignation, and becomes prevailed upon to
            restore Claudia to her household Gods (her home), and that
            the Emperor may know of this insurrection, from my own lips,
            I will make no further delay in carrying out the
            instructions I have received from the Prefect.

:CHORUS: Why dost thou bring about all this cruel strife? it is of no
         good! Cupid is invincible, and has used those arrows of his,
         which will obscure all thy fires (throw them into the shade).
         Will the flames which he has set up in the heart of Nero ever
         be cooled down? That little Deity has drawn down even Jupiter
         himself from lofty Olympus, and has extinguished his very
         lightning. Thou wilt pay with thy life any obstacle thou mayst
         throw in his way; he is hot in his rage and not very patient,
         in his transports of anger, or easy to be brought under
         control.—He, it was, who commanded that ferocious Achilles, to
         strike his lyre, and produce his amorous melodies, he it was
         who was the means of nearly ruining the Greeks with their ten
         years’ war—he it was who paved the way for the downfall of
         Agamemnon—he it was who destroyed the kingdom of poor old
         Priam, and has been the means of ruining the beautiful cities
         of the world; and now our minds are simply horrified at what he
         can really do, and at the unrelenting energy now being
         displayed by that merciless little God!

ACT V.
------

:NERO—PREFECT: (Nero, boiling over with rage, on account of the
                 tumultuous rising of the populace, orders the most
                 severe measures to be taken against them, and that
                 Octavia, as the cause of such a rising, shall be
                 transported to Pandataria and there slain.)

:NERO: OH! the excessive laggardliness in the spirit of my soldiery, and
       oh!  what anger rages within me, suffering as I have done from
       the commission of such dreadful crimes! Why has not the very
       life-blood of the citizens been made to extinguish the torches
       which have been kindled against me for my destruction? Why does
       Rome, assuming such a funeral aspect, not wade in the blood
       arising from the slaughter of such a populace? Oh thou Rome! that
       has ever produced men like them! but it would be a trifling thing
       for them to be punished only with that death that is the admitted
       retribution ordained for such deeds. No! this impious crime of
       the populace deserves more than that! But she, Octavia, for whom
       the fury of the citizens has subjected me to all this, and who
       has always been as a sister and wife to me, but whom I have had
       every reason to suspect, she shall at last be made to give up her
       life to me as the cost of that just anger, which she has always
       excited in my bosom, and she shall extinguish that anger with her
       blood! Very soon, the homesteads of the citizens shall fall
       a prey to the conflagrations which I will set going!  Fire, utter
       ruin, shall weigh down this hateful rabble, extremest privations,
       bitter starvation with weeping and sorrow! The fact is a large
       proportion of the citizens have been eaten up with corruption and
       idleness and have grown exultant and surfeited with all the
       benefits that have accrued to them during my reign, nor does the
       ungrateful rabble appreciate the clemency they have received
       during my beneficent rule, nor, further, can they bear the idea
       of things going on peaceably, but the restless rascals must be
       seized with some mania or another, and in one direction they are
       carried away by sheer audacity, and in another they drift
       headlong with their rashness! These men must be kept under by
       terrible punishments, and perpetually weighed down by some
       oppressive yoke, lest they may have the audacity to venture upon
       a repetition of those outrages at some future time! No! they
       shall be made to raise their eyes with reverential respect at the
       divine face of my wife, and being crushed by the fear of my
       punishments, to obey the very nod of their emperor! But I now see
       coming towards me, a man, whose strict habits of discipline and
       acknowledged fidelity to my sceptre, have installed him in his
       present high position in my camp.

:PREFECT: I have to report that the fury of the populace has at last
          been brought under, with the slaughter, too, of only some few,
          who, for a time, resisted to the last, urged on by their
          foolish obstinacy.

:NERO: And is this, dost thou suppose, enough? Is this, too, the mode in
       which thou, as a soldier, hast dared to address thy Emperor?
       Thou appeasedst them indeed! No! No! let this hostile little
       modicum of punishment business fall to my lot!

:PREFECT: The wicked leaders of the insurrection have already fallen by
          the sword.

:NERO: What! that rascally rabble that dared to seek out my very Palace,
       and consign it to the flames; in other words, to lay down the law
       to their very Emperor, and to drag away my darling wife from my
       lawfully instituted marriage couch, to violate her liberty in
       short, as far as was in their power, by their incestuous hands
       and terrifying language!  No! the punishment which they deserve
       must be left for me to carry out.

:PREFECT: Will thy anger determine thee to inflict still further
          punishment upon thy citizens?

:NERO: My anger will determine me to inflict that punishment which no
       length of time will ever serve to efface from the memory of man.

:PREFECT: But canst thou not determine some punishment which will impose
          some sort of limit to thy anger, and which, at the same time,
          would diminish our fears.

:NERO: The first object that shall expiate my anger, will be that one
       who deserves it the most.

:PREFECT: Tell me whom thou wilt require for that purpose, and do not
          let our hands spare them.

:NERO: My anger demands the execution of my sister, I require her odious
       life to be taken away.

:PREFECT: I am trembling with horror at thy words—a sudden rigor has
          frozen up my veins! I am spell-bound!

:NERO: Dost thou hesitate, then, to obey?

:PREFECT: Why shouldst thou call my fidelity into question?

:NERO: Why wouldst thou appear inclined to spare an enemy?

:PREFECT: Dost thou mean to say, that any woman, as far as thou art
          concerned, deserves such a name as enemy?

:NERO: Not if she has lent herself to acts of crime?

:PREFECT: Is there anyone who can prove Octavia to be guilty of that?

:NERO: This fury of the populace amply proves it to me.

:PREFECT: Who is able, to exercise any influence over a lot of madmen?

:NERO: Octavia, who was the means of exciting them on to those crimes.

:PREFECT: I cannot suppose any woman to be capable of such a thing!

:NERO: A woman, in whom nature has implanted the disposition, prone to
       do evil, and which has endowed her mind with all the instincts of
       crime and treachery, but yet that nature has withheld from her
       the requisite power, so that she should not in short be so
       impregnable, but that fear might have some chance of breaking
       down her feeble powers for mischief, or the punishment itself,
       which, although late in the day, threatens to be visited upon
       her, now that she is finally condemned, but this only, after
       having been an offender for so long! Therefore, abstain from
       offering me any more suggestions, or advancing any more
       intercessions, and see and carry out my orders to the very
       letter; give orders that Octavia be carried away, in some craft
       or other, to a remote spot, to some far-off shore, that, at last,
       the surging wrath in my breast may be allowed to cool down!


:CHORUS—OCTAVIA.: The Chorus sings regarding popular favor, which has
                    been destructive to so many, and after that, brings
                    into notice, the hard fates which have befallen the
                    Caesarean Dynasty.

:CHORUS: OH! that favor and enthusiastic preference emanating, from the
         people!  What a source of trouble, and misery it has proved to
         so many! It is like the craft, which has filled its sails under
         a favorable wind, and has carried thee far away from the shore,
         but which same wind, when a dead calm presents itself, leaves
         thee helpless in the cruel ocean depths! A miserable parent,
         [#]_ aforetime bewailed the loss of the Gracchi (Cornelia) whom
         intense popular regard, and excessive appreciation by the
         public, were the means of leading to their ultimate ruin,—men,
         too, of such illustrious descent, and acknowledged piety,
         fidelity, distinguished eloquence, moral courage, and of
         unflinching severity, in their administration of just laws; and
         thee also Livius, [#]_ fortune gave up to a similar end, whom
         neither thy magisterial dignity, nor the roof of thy very
         homestead, served as a protection against death! We could
         adduce many more striking examples, if our griefs did not
         prevent us—it was only quite lately, Octavia, that citizens
         were up in arms, and were most desirous of restoring to thee
         thy country—thy palace, and to exact from thy brother thy
         conjugal rights, but now, forsooth, they can calmly look on and
         see thee weeping and in misery—dragged away to meet thy doom!
         Poverty, in a state of happy contentment, lies hidden under the
         humble roof, but the storms of fate shake the lofty palaces, or
         capricious fortune overthrows them altogether!

:OCTAVIA: Where art thou conducting me? What has that tyrant Nero
          ordered now? or what exile has his Queen Poppaeea appointed
          for me? or is it that she is melted by compassion at the
          troubles I have suffered, and my being so utterly cast down by
          such an array of misfortunes? If Nero is preparing to
          accumulate my sorrows, by my slaughter, as a climax to my
          sufferings, why does he even grudge me the privilege of dying
          in my own paternal soil, although my country has been the
          arena of so much cruelty towards me? But now there is no
          apparent hope of my ultimate safety—I perceive already in my
          misery the craft which bore away my brother! Ah! that is the
          craft, too, in which his mother was once carried off, and now,
          as an unfortunate wretch, banished from the marriage bed,
          I shall be carried away by the same conveyance. Piety has no
          tutelar deity now, and the Gods above, alas! are nowhere to be
          found!  It is that cruel Erinnys, who can now cause me to weep
          adequately for the evils I have gone through! What Thracian
          nightingale will ever send forth its plaintive notes equal to
          mine? I only wish the Fates would give to me, in misery,
          a pair of wings! would I not cleave the air with my rapid
          wings spread out, and fly far, far away from all my present
          troubles, and remote from the busy haunts of man, and the
          hotbed of cruel slaughter, and alone in the desert grove,
          perched on some delicate twig, should I then be able to warble
          my tristful strains from my sorrowing throat!

:CHORUS: The race of mortals is governed by the inexorable Fates!  Nor
         does any thing sublunary answer the expectations of anyone as
         regards stableness or durability! and the coming day is always
         to be dreaded; whilst it invariably brings round in its train,
         such a variety of events! Surely thy Caesarean dynasty has
         undergone many troubles!  What! Is fortune more cruel to thee,
         than it has been to many others before thee? We will mention
         thee, first of all, oh! thou the daughter of Agrippa, the
         unhappy parent of so many sons, the daughter-in-law of an
         Augustus, the wife of a Caesar, whose name shone so gloriously
         over the whole world, thou, that broughtest forth, from thy
         gravid uterus, so many pledges of peace to the universe!
         a double pledge, first, of love to a husband, secondly,
         a guarantee of unbroken succession to the imperial throne; by
         and bye, exile, stripes, undergoing the indignity of being
         fettered by chains, and being thus tormented for a long time,
         the once felicitously married Livia, [#]_ the wife of Drusus,
         happy too, with the possession of her sons, rushed on to the
         commission of a terrible crime, and its subsequent punishment!
         Julia, [#]_ her daughter, followed the fate of her mother;
         after a long time, however, she met her death by the sword,
         although for no crime, of her own! What could not thy own
         mother, Messalina, do who filled the palace of the Emperor, so
         dear to that husband too, and so proud and elated with her
         progeny; yet this same woman, having submitted to the unlawful
         advances of an underling (the marriage with Silius), fell by
         the sword of a savage soldier! What about Agrippina, too, such
         an illustrious parent of thy own Nero, who, with justice, and
         every show of reason, could have aspired to a place in the
         heavens, to absolute Apotheosis, as Divus did! was she not,
         however, outraged by the terrible hands of the Tyrrhenian
         boatmen, before she was seen to be hacked about by the sword,
         for a considerable time, and eventually succumbed, as the
         victim of a cruel son!

:OCTAVIA: Behold that cruel tyrant will send me likewise to the tristful
          shades, and the manes! Why in my misery am I detained on earth
          to no purpose?  Let me be seized upon for one of death’s
          victims, by those to whose power my bitter lot has surrendered
          me! I call the gods above to witness! But what am I now
          talking about in my madness? Let me spare myself the mockery
          of invoking the good will of the deities to whom, for some
          cause or other, I have evidently been an object of hatred!
          I therefore call the deities of Hell to witness, and the
          goddesses of Erebus, who are the avengers of crime, and thee,
          even, oh!  my father, who really wert worthy of such a death,
          and punishment, as I am now about to suffer from—that death,
          however, is by no means unacceptable or hateful to me—Get the
          craft in readiness, unfurl the sails, and commit her to the
          waves and let the commander of that craft steer for the coast
          of Pandataria with a flowing breeze!

:CHORUS: Oh! for the gentle breezes. Oh! for the light and balmy
         Zephyrs, which caught thee up, and wafted thee away, Iphigenia,
         surrounded, by an ethereal cloud, far from the altars of the
         cruel goddess (Diana), Oh! ye kind breezes, convey away this
         victim, Octavia, far away, from any cruel punishment, I pray,
         to the temples of Trivia, even (Diana) Aulis itself, is a less
         cruel place than thy city of Rome, and so is the land of the
         Tauri, [#]_ for there it is they sacrifice the blood of any
         strangers who approach their shores, to appease the anger of
         the goddess whom they worship! But Rome is very different, she
         rejoices only in the slaughter of her own citizens!

.. [#] GENITRIX.—Urgulanilla and Aelia Paetina were divorced by Claudius
    before he married Messalina. Messalina the mother of Octavia, was
    noted for her lustful propensities, supposedly, I should think,
    suffering from the “furor uterinus”, which was not very mercifully
    regarded in those days. At all events, consistent with this notion
    of nymphomania, which led to such doings, so derogatory to her
    dignity as a Queen Consort, she had been guilty of a series of
    immoralities, before the disgraceful mockery of marriage with
    Silius, which, this time, however, cost her her life.


.. [#] NOVERCA.—The marriage of Claudius with Agrippina was regarded
    in Rome, as an incestuous marriage, although according to `Juvenal
    <../juvenalx.html>`__, sexual morality was not a canon held in the
    strictest observance in those days of Patrician licentiousness.

.. [#] ELECTRA.—Sophocles has alluded copiously to the weeping of
    Electra, and her strong desire for the return of Orestes, to revenge
    the death of their father, Agamemnon.

.. [#] SUBJECTA FAMULAE.—Seneca constantly uses this word and in very
    different senses. Poppaea was not a slave, but a woman of good
    descent.  Her father had filled the office of Quaestor.

.. [#] SILANUS.—Silanus was not killed, but committed suicide, the
    same day that Claudius married Agrippina, and `Tacitus
    <../tacitusx.html>`__ says this added to the public indignation.

.. [#] VIDIMUS COELO JUBAR.—`Tacitus <../tacitusx.html>`__ alludes to
    this comet, and `Seneca <../seneca_youngerx.html>`__ in the Quaest:
    Natur.

.. [#] REMOTUS.—`Seneca <../seneca_youngerx.html>`__ had been accused
    of adultery with Julia, the daughter of `Germanicus
    <../germanicusx.html>`__, and was expatriated by Claudius to the
    island of Corsica. Agrippina obtained his return and made him the
    tutor of Nero.

.. [#] PLAUTI SULLAEQUE.—Plautus Rabellius had been exiled into Asia,
    and Sulla into Narbonensian Gaul: but they were both executed by
    Nero’s orders—`Tacitus <../tacitusx.html>`__, Lib. 12. Annal., and
    `Suetonius <../suetoniusx.html>`__ apud Neronem, Cap. 15.

.. [#] PAVERE.—It was at Philippi, where a great battle was fought by
    Octavius and Antony, against Brutus and Cassius, and allusion is
    here made to the immense number of the slain, which were left
    exposed, unburied, on the plains for the birds of prey to feast
    upon.

.. [#] CULPA SENECAE.—I think that the rendering I have given of the
    word “Culpa”, represents the poet’s meaning.

.. [#] MISERANDA PARENS.—This unfortunate woman was Cornelia, the
    daughter of Scipio Africanus, and being sprung from him, was
    consequently a scion of one of the principal families in Rome.

.. [#] TE QUOQUE LIVI.—The tribune Livius Drusus, established great
    reforms in the laws. He was assassinated just as he was leaving his
    own house.

.. [#] LIVIA.—Livia poisoned her husband, Drusus.

.. [#] JULIA.—Julia, the daughter of Livia, was accused of
    complicity in the poisoning of Drusus, but it was not proved; she
    was, nevertheless, exiled and ultimately suffered death.

.. [#] TAURORUM.—The Tauri were a people of Scythia, and they
    sacrificed strangers on the altars of Diana.
